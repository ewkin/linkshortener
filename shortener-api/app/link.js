const express = require('express');
const Link = require("../models/Link");
const {nanoid} = require("nanoid");
const router = express.Router();




router.post('/', async (req, res) => {
    try {
        const linkData = req.body;
        linkData.shortUrl = nanoid(6);
        const link = new Link(linkData);
        await link.save();
        res.send(link);
    } catch (e) {
        res.sendStatus(500)
    }
});

module.exports = router;