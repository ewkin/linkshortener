const express = require('express');
const cors = require("cors");
const link = require('./app/link');
const Link = require("./models/Link");
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/link', link);

app.get('/:id', async (req, res) => {
    console.log(req.params.id);
    try {
        const link = await Link.findOne({shortUrl: req.params.id});

        if (link) {
            res.status(301).redirect(link.originalUrl);
        } else {
            res.sendStatus(404);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

const run = async () => {
    await mongoose.connect('mongodb://localhost/shop', {useNewUrlParser: true, useUnifiedTopology: true});
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async callback => {
        await mongoose.disconnect();
        console.log('Mongoose disconnected');
        callback();
    });
}
run().catch(console.error);