import {FETCH_SHORTENER_FAILURE, FETCH_SHORTENER_REQUEST, FETCH_SHORTENER_SUCCESS} from "../actions/shortenerActions";

const initialState = {
    link: {},
    error: false
};

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SHORTENER_REQUEST:
            return {...state, error: false};
        case FETCH_SHORTENER_SUCCESS:
            return {...state, link: action.link};
        case FETCH_SHORTENER_FAILURE:
            return {...state, error: true};
        default:
            return state;
    }
};

export default productReducer;