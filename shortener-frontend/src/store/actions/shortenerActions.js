import axiosApi from "../../axiosApi";

export const FETCH_SHORTENER_REQUEST = 'FETCH_SHORTENER_REQUEST';
export const FETCH_SHORTENER_SUCCESS = 'FETCH_SHORTENER_SUCCESS';
export const FETCH_SHORTENER_FAILURE = 'FETCH_SHORTENER_FAILURE';

export const fetchShortenRequest = () => ({type: FETCH_SHORTENER_REQUEST});
export const fetchShortenSuccess = link => ({type: FETCH_SHORTENER_SUCCESS, link});
export const fetchShortenFailure = () => ({type: FETCH_SHORTENER_FAILURE});

export const createShortLink = link => {
    return async dispatch => {
        try {
            dispatch(fetchShortenRequest());
            const response = await axiosApi.post('/link', link);
            dispatch(fetchShortenSuccess(response.data));
        } catch (e) {
            dispatch(fetchShortenFailure());
        }
    };
};