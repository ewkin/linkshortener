import React from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";

import LinkShortener from "./contaners/linkShortener/linkShortener";


const App = () => (
    <>
        <CssBaseline/>
        <main>
            <Container maxWidth="xl">
                <LinkShortener/>
            </Container>
        </main>
    </>
);

export default App;
