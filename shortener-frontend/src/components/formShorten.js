import React, {useState} from 'react';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";

const FormShorten = ({onSubmit}) => {
    const [state, setState] = useState({
        originalUrl:''
    });

    const submitFormHandler = e => {
        e.preventDefault();
        onSubmit(state);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => ({
            ...prevState, [name]: value
        }));
    };

    return (
        <form onSubmit={submitFormHandler}>
            <Grid container justify="center" spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        variant="outlined"
                        id="originalUrl"
                        label="URL"
                        name="originalUrl"
                        value={state.originalUrl}
                        onChange={inputChangeHandler}/>
                </Grid>
                <Grid  item xs={3}>
                    <Button type="submit" color="primary" variant="contained">
                        Shorten!
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default FormShorten;