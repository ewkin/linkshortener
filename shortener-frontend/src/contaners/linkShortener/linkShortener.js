import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import FormShorten from "../../components/formShorten";
import {createShortLink} from "../../store/actions/shortenerActions";
import {apiURL} from "../../config";

const useStyles = makeStyles(theme => ({
    holder: {
        marginTop: 100,
        background: 'white',
    },
    name: {
        textAlign: 'center',
        fontSize: 40,
    },
    link: {
        textAlign: 'center',
        fontSize: 25,
    }
}));

const LinkShortener = () => {
        const classes = useStyles();
        const dispatch = useDispatch();
        const link = useSelector(state => state.link.shortUrl);

        const submitUrl = (url) => {
            dispatch(createShortLink(url));
        };

        return (
            <Grid container justify="center" className={classes.holder} spacing={3}>
                <Grid item xs={12}>
                    <Paper className={classes.name} elevation={0}>Shorten your link!</Paper>
                </Grid>
                <Grid item xs={6}>
                    <FormShorten
                        onSubmit={submitUrl}/>
                </Grid>
                {link && <Grid item xs={12}>
                    <Paper className={classes.link} elevation={0}>Your new link looks like this:</Paper>
                    <Paper className={classes.link} elevation={0}>
                        <a className={classes.link} href={apiURL + '/' + link}>{apiURL + '/' + link}
                        </a>
                    </Paper>
                </Grid>}

            </Grid>
        );
    }
;

export default LinkShortener;